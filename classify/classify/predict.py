from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import numpy as np
from keras.preprocessing.image import ImageDataGenerator

import keras
from tensorflow.keras.preprocessing import image
import warnings
warnings.filterwarnings("ignore")
from django.conf import settings





def prepare_imager(file):
    img = image.load_img(file, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    return keras.applications.resnet50.preprocess_input(img_array_expanded_dims)


def predr(prediction):
    fruit = ['Blueberry', 'Orange', 'Corn', 'Cocos']
    
    
    conf = settings.MODEL.predict(prediction)[0]

    idx = np.argmax(conf)
    label = fruit[idx]

    label = "{}".format(label)
    return str(label)

