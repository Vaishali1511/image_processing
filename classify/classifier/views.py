from django.shortcuts import render


from django.http import JsonResponse
import base64
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.conf import settings 

from django.core.files.storage import FileSystemStorage


import matplotlib.pyplot as plt
import numpy as np

import datetime
import traceback

from tensorflow.keras.preprocessing.image import img_to_array

import numpy as np
from keras.preprocessing.image import ImageDataGenerator

import keras
from tensorflow.keras.preprocessing import image
from classify.predict import prepare_imager,predr

def index(request):
    if  request.method == "POST":
        f=request.FILES['sentFile'] # here you get the files needed
        response = {}
        fs=FileSystemStorage()
        filePathName=fs.save(f.name,f)
        filePathName=fs.url(filePathName)
        testimage='.'+filePathName
        
        
        processed_image = prepare_imager(testimage)
        
        # get the predicted probabilities for each class
        
        predictions=predr(processed_image)
       
        
        response['name'] = str(predictions)
        return render(request,'homepage.html',response)
    else:
        return render(request,'homepage.html')
